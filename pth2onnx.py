import torch
import torch.onnx
from model_building import SynergyNet

import argparse

device = torch.device("cuda:0" if torch.cuda.is_available() else "cpu")

    
if __name__ == "__main__":
 #test()
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    args.arch = 'mobilenet_v2'
    args.img_size = 120

    pthfile = 'pretrained/best.pth.tar' 
    checkpoint = torch.load(pthfile, map_location='cpu')['state_dict']

    model = SynergyNet(args)
    model_dict = model.state_dict()

    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    print("loading weights successfully...")
    model.load_state_dict(model_dict, strict=False)#(loaded_model['net'])
    model.eval()

    dummy_input1 = torch.randn(1,3,120,120)
    dummy_input1 = dummy_input1.unsqueeze(0)

    input_names = [ "actual_input"]
    output_names = [ "output1","output2" ]

    torch.onnx.export(model, dummy_input1, "synergy_weights_cpu.onnx", verbose=True, input_names=input_names, output_names=output_names)
    print("pth model convert to onnx successfully")
