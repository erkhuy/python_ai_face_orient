import torch
import torchvision.transforms as transforms
import numpy as np
import cv2
from utils.ddfa import ToTensor, Normalize
from model_building import SynergyNet
from utils.inference import crop_img, predict_sparseVert, predict_denseVert, predict_pose, draw_axis
import argparse

from fastapi import File, UploadFile, FastAPI
import uvicorn
import onnxruntime as ort 

import sys 
sys.path.append('FaceBoxes')

from FaceBoxes.layers.functions.prior_box import PriorBox
from FaceBoxes.utils.box_utils import decode
from FaceBoxes.data import cfg
from FaceBoxes.utils.nms_wrapper import nms

IMG_SIZE = 120
app = FastAPI()

def init_model_synergy(path):
    parser = argparse.ArgumentParser()
    args = parser.parse_args()
    args.arch = 'mobilenet_v2'
    args.img_size = IMG_SIZE
    # args.devices_id = None

    checkpoint_fp = path 

    checkpoint = torch.load(checkpoint_fp, map_location='cpu')['state_dict']

    model = SynergyNet(args)
    model_dict = model.state_dict()

    for k in checkpoint.keys():
        model_dict[k.replace('module.', '')] = checkpoint[k]

    model.load_state_dict(model_dict, strict=False)
    # model = model.cuda()
    model.eval()

    return model 

def face_detect(img_raw, ort_session, device):
    score_thr = 0.5
    
    origin_h, origin_w = img_raw.shape[:2]
    img = np.float32(img_raw)
    img = cv2.resize(img, (960, 960))
    im_height, im_width = img.shape[:2]
    dh = origin_h/960
    dw = origin_w/960

    scale = torch.Tensor([im_width, im_height, im_width, im_height])

    img -= (104, 117, 123)
    img = img.transpose(2, 0, 1)
    img = torch.from_numpy(img).unsqueeze(0)
    img = img.numpy()
    scale = scale.to(device)

    outname = [i.name for i in ort_session.get_outputs()]
    inname = [i.name for i in ort_session.get_inputs()]
    inp = {inname[0]:img}

    loc, conf = ort_session.run(outname, inp)

    priorbox = PriorBox(cfg, image_size=(im_height, im_width))
    priors = priorbox.forward()
    priors = priors.to(device)
    prior_data = priors.data
    
    boxes = decode(torch.from_numpy(np.asarray(loc.data)).squeeze(0).to(device), prior_data, cfg['variance'])
    boxes = boxes * scale 
    boxes = boxes.cpu().numpy()
    scores = torch.from_numpy(np.asarray(conf)).squeeze(0).data.cpu().numpy()[:, 1]

    inds = np.where(scores > score_thr)[0]
    boxes = boxes[inds]
    scores = scores[inds]

    order = scores.argsort()[::-1][:5000]
    boxes = boxes[order]
    scores = scores[order]

    dets = np.hstack((boxes, scores[:, np.newaxis])).astype(np.float32, copy=False)
    keep = nms(dets, 0.3)
    dets = dets[keep, :]

    dets = dets[:750, :]

    boxes_detect = []
    for k in range(dets.shape[0]):
        xmin = int(max(min(dets[k, 0]*dw, origin_w), 0)) 
        ymin = int(max(min(dets[k, 1]*dh, origin_h), 0)) 
        xmax = int(max(min(dets[k, 2]*dw, origin_w), 0)) 
        ymax = int(max(min(dets[k, 3]*dh, origin_h), 0)) 
        score = dets[k, 4]
        boxes_detect.append([xmin, ymin, xmax, ymax, score])
    return boxes_detect

def init_face_boxes_onnx(path):
    ort_session = ort.InferenceSession(path, providers=['CPUExecutionProvider'])
    return ort_session

def detect(img_ori, synergy_model, fb_model, device):

    rects = face_detect(img_ori, fb_model, device)

    transform = transforms.Compose([ToTensor(), Normalize(mean=127.5, std=128)])
    pts_res = []
    poses = []
    vertices_lst = []
    for rect in rects:
        roi_box = rect 
        HCenter = (rect[1] + rect[3])/2
        WCenter = (rect[0] + rect[2])/2
        side_len = roi_box[3]-roi_box[1]
        margin = side_len * 1.2 // 2
        roi_box[0], roi_box[1], roi_box[2], roi_box[3] = WCenter-margin, HCenter-margin, WCenter+margin, HCenter+margin

        img = crop_img(img_ori, roi_box)
        img = cv2.resize(img, dsize=(IMG_SIZE, IMG_SIZE), interpolation=cv2.INTER_LINEAR)

        input = transform(img).unsqueeze(0)
        with torch.no_grad():
            # input = input.cuda()
            param = synergy_model.forward_test(input)
            param = param.squeeze().cpu().numpy().flatten().astype(np.float32)

        lmks = predict_sparseVert(param, roi_box, transform=True)
        vertices = predict_denseVert(param, roi_box, transform=True)
        angles, translation = predict_pose(param, roi_box)

        pts_res.append(lmks)
        vertices_lst.append(vertices)
        poses.append([angles, translation, lmks])
    
    return pts_res, poses, vertices_lst

def draw_orient(img_ori, poses):
    img_axis_plot = img_ori.copy()
    for angles, translation, lmks in poses:
            img_axis_plot = draw_axis(img_axis_plot, angles[0], angles[1],
                angles[2], translation[0], translation[1], size = 50, pts68=lmks)

def check_orient(poses):
    if len(poses) == 0:
        return 'Looking Back'
    pose = ''
    for angles, translation, lmks in poses:
        if angles[0] <= -45 :
            pose = 'Looking Left'
        elif angles[0] >= 45 :
            pose = 'Looking Right'
        elif angles[0] < 45 and angles[0] > -45 :
            pose = 'Looking Forward'
    return pose 

synergy_model = init_model_synergy('pretrained/best.pth.tar')
face_boxes_model = init_face_boxes_onnx('faceboxes_weights_cpu.onnx')
device = torch.device('cpu')

@app.post("/get_face_orient")
async def get_face_orient(img: UploadFile = File(...)):
    try:
        global synergy_model
        global face_boxes_model
        global device 

        data_img = img.file.read()
        img = cv2.imdecode(np.fromstring(data_img, np.uint8), cv2.IMREAD_UNCHANGED)

        _, poses, _ = detect(img, synergy_model, face_boxes_model, device)
        orient = check_orient(poses)
        
        data = {'Orient': orient}
        # draw_orient(img, poses)
        return data
    except:
        return {'success': 0}

def main():
    uvicorn.run(app, host="0.0.0.0", port=8032)

if __name__ == '__main__':
    main()
    # img = cv2.imread('hoa.png')
    # _, poses, _ = detect(img, synergy_model, face_boxes_model, device)
    # orient = check_orient(poses)
    # print(orient)
